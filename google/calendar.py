#!/bin/python3
from __future__ import print_function

import json
import os
import datetime
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
SCOPES = ['https://www.googleapis.com/auth/calendar']

def googleCreds():
  # If modifying these scopes, delete the file token.json.
  creds = None
  # The file token.json stores the user's access and refresh tokens, and is
  # created automatically when the authorization flow completes for the first
  # time.
  if os.path.exists('token.json'):
      creds = Credentials.from_authorized_user_file('token.json', SCOPES)
  # If there are no (valid) credentials available, let the user log in.
  if not creds or not creds.valid:
      if creds and creds.expired and creds.refresh_token:
          creds.refresh(Request())
      else:
          flow = InstalledAppFlow.from_client_secrets_file(
              '/home/kyabe/openIT/calendar/google/credentials.json', SCOPES)
          creds = flow.run_local_server(port=0)
      # Save the credentials for the next run
      with open('token.json', 'w') as token:
          token.write(creds.to_json())
  
          
def create_google_event(event_info, calendarId):
  if not os.path.exists('token.json'):
    googleCreds()
  creds = Credentials.from_authorized_user_file('token.json', SCOPES)
  service = build('calendar', 'v3', credentials=creds) 

  event_body = {
    'summary': event_info["session_name"],
    'location': '96 Rue Icare, 34130 Mauguio',
    'start': {
      'dateTime': event_info["start_date"],
      'timeZone': 'Europe/Paris'
    },
    'end': {
      'dateTime': event_info["end_date"],
      'timeZone': 'Europe/Paris'
    },
    'attendees': [
      {'email': 'n34.rigoli@gmail.com'}
    ]
  }
#https://pynative.com/python-serialize-datetime-into-json/  
  class DateTimeEncoder(json.JSONEncoder):
        #Override the default method
        def default(self, obj):
            if isinstance(obj, (datetime.date, datetime.datetime)):
                return obj.isoformat()
              
  event_body = json.dumps(event_body, ensure_ascii=True, indent=1, cls=DateTimeEncoder) 
  #print(json.loads(event_body))
  
  event = service.events().insert(calendarId=calendarId, body=json.loads(event_body)).execute()
  print(f"Event created:{event.get('htmlLink')}")  
  
def listEvent(calendarId): 
  if not os.path.exists('token.json'):
    googleCreds()
  creds = Credentials.from_authorized_user_file('token.json', SCOPES)
  service = build('calendar', 'v3', credentials=creds)   
  page_token = None
  eventsReturned = []
  while True:
    events = service.events().list(calendarId=calendarId, pageToken=page_token).execute()
    for event in events['items']:
      eventsReturned.append(event)
    page_token = events.get('nextPageToken')
    if not page_token:
      break
  return eventsReturned

def deleteEvents(evenList, calendarID):
  if not os.path.exists('token.json'):
    googleCreds()
  creds = Credentials.from_authorized_user_file('token.json', SCOPES)
  service = build('calendar', 'v3', credentials=creds)   
  for event in evenList:
    #print(event)
    service.events().delete(calendarId=calendarID, eventId=event["id"]).execute()
    
def resetCalendar(calendarId):
  eventList = listEvent(calendarId)
  deleteEvents(eventList, calendarId)
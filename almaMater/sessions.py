#!/bin/python3
import json
import requests
import datetime
from ast import Constant
from dotenv import load_dotenv
from dateutil import tz

load_dotenv()

def login(base_url, email, password):
  URL =  Constant(f"{base_url}/actor/login_extranet")
  payload = json.dumps({"email": email,"password": password})
  headers = {
    'Content-Type': 'application/json'
  }

  response = requests.request("POST", URL.value, headers=headers, data=payload)

  if(response.status_code == 200):
    return(json.loads(response.text))

def getSessions(base_url, bearer_token):
  entity_type = "student"
  entity_id = "148214"
  url = f"{base_url}/erp/session_course/module-user/6/148214?entity_type={entity_type}&entity_id={entity_id}"

  headers = {"Authorization": bearer_token, 'Cookie': 'x-token=s%3AK0IxI-wIdoDvu_H6yKW_Gbu-Ui_Sr75X.TTQe9fXF%2BRNpGmnlcW%2FBQvbUcOVpoQFVHnfJ0kULIq4'}
  response = requests.request("GET", url, headers=headers)
  
  if(response.status_code == 200):
    return(json.loads(response.text))
  
def get_future_sessions(sessions):
  future_lessons = []
  from_zone = tz.gettz('UTC')
  to_zone = tz.gettz('Europe/Paris')
  for session in sessions:
    session
    format = "%Y-%m-%dT%H:%M:%S.%fZ"
    
    start_date_utc= datetime.datetime.strptime(session["event"].get("start"), format)
    end_date_utc = datetime.datetime.strptime(session["event"].get("end"), format)
    start_date_utc = start_date_utc.replace(tzinfo=from_zone)
    end_date_utc = end_date_utc.replace(tzinfo=from_zone)
    
    start_date_paris = start_date_utc.astimezone(to_zone)
    end_date_paris = end_date_utc.astimezone(to_zone)
    #print(start_date_paris)
    if(start_date_paris >=  datetime.datetime.now(tz=to_zone)):
      # print("***********")
      # print(session["label"])
      # print(f"start: {start_date}")
      # print(f"end: {end_date}")
      future_lessons.append({"session_name": session["label"], "start_date": start_date_paris, "end_date": end_date_paris})
  return(future_lessons)
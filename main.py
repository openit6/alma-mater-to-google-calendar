#!/bin/python3
import os
from ast import Constant
from dotenv import load_dotenv
import almaMater.sessions
import google.calendar

load_dotenv()


if __name__ == '__main__':
  EMAIL = os.getenv("EMAIL")
  PASSWORD = os.getenv("PASSWORD")
  CALENDAR = os.getenv("CALENDAR")
  BASE_URL=os.getenv("BASE_URL")

  google.calendar.googleCreds()
  google.calendar.resetCalendar(CALENDAR)
  
  tokens = almaMater.sessions.login(BASE_URL, EMAIL, PASSWORD)
  bearer_token  = Constant(tokens["bearerToken"])
  sessions = almaMater.sessions.getSessions(BASE_URL, bearer_token.value)
  future_lessons = almaMater.sessions.get_future_sessions(sessions)
  #print(future_lessons)
  for lesson in future_lessons:
    google.calendar.create_google_event(lesson, CALENDAR)